package com.kenneth.event.kafka.services;

import com.kenneth.event.kafka.models.Dummy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class ProducerService {

    @Autowired
    private KafkaTemplate<String, Serializable> kafkaTemplate;

    public void sendData(Dummy data){
        kafkaTemplate.send("crypto", 0, null, data);
    }
}
