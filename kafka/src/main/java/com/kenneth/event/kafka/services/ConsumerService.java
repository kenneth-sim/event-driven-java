package com.kenneth.event.kafka.services;

import com.kenneth.event.kafka.models.Dummy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ConsumerService {

    @RetryableTopic(attempts = "4", backoff = @Backoff(delay = 3000, multiplier = 1.5, maxDelay = 15000), exclude = {NullPointerException.class}) // 3 topic N-1
    @KafkaListener(topics = "crypto", groupId = "company1")
    public void listenGroupCompany1(Dummy message, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic, @Header(KafkaHeaders.OFFSET) String offset) {
        log.info("Company1 Received Message: {} topic: {} offset: {}", message, topic, offset);
    }

    @KafkaListener(topics = "crypto", groupId = "company1Partition0", topicPartitions = @TopicPartition(topic = "crypto", partitions = {"0"}))
    public void listenGroupCompany1Partition0(Dummy message, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic, @Header(KafkaHeaders.OFFSET) String offset) {
        log.info("Company1Partition0 Received Message: {} topic: {} offset: {}", message, topic, offset);
    }

    @DltHandler
    public void listenDLT(Dummy message, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic, @Header(KafkaHeaders.OFFSET) String offset){
        log.info("DLT Received Message: {} topic: {} offset: {}", message, topic, offset);
    }
}
