package com.kenneth.event.kafka.controllers;

import com.kenneth.event.kafka.models.Dummy;
import com.kenneth.event.kafka.services.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

@RestController
@RequestMapping("/kafka")
public class TestingKafkaController {

    @Autowired
    private ProducerService producerService;

    @PostMapping("/crypto")
    public void pushCryptoData(){
        Dummy data  = Dummy.builder().name("BTC").price(BigDecimal.valueOf(new Random().nextDouble())).ts(new Date().getTime()).build();
        producerService.sendData(data);
    }
}

