package com.kenneth.event.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaApplication {

//    https://haithai91.medium.com/zookeeper-kafka-with-docker-b1d4b33c0d5e
    public static void main(String[] args) {
        SpringApplication.run(KafkaApplication.class, args);
    }
}
