package com.kenneth.nettytcpserver.tcp;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ChatServer implements CommandLineRunner {
    EventLoopGroup bossGroup;
    EventLoopGroup workerGroup;

    @Value("${chatServer.netty.port:8001}")
    private int port;

    @Override
    public void run(String... args) throws Exception {
        bossGroup = new NioEventLoopGroup(); //accept incoming connections
        workerGroup = new NioEventLoopGroup(); //for processing

        try{
            //use ServerBootstrap to define how the server will process incoming connections
            ServerBootstrap bootstrap = new ServerBootstrap()
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class) //use this server socket for communication
                    .childHandler(new ChatServerInitializer());

            //ask bootstrap to bind with the specified port and start listening to incoming connections
            bootstrap.bind(port).sync().channel().closeFuture().sync();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }finally {
            if (bossGroup != null) {
                bossGroup.shutdownGracefully().sync();
            }

            if (workerGroup != null) {
                workerGroup.shutdownGracefully().sync();
            }
        }
    }
}
