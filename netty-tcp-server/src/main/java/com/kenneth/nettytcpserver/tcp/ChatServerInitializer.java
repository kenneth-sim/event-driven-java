package com.kenneth.nettytcpserver.tcp;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.springframework.stereotype.Component;

@Component
public class ChatServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();

        //tell Netty we're expecting frames of at most 8192 in size, each delimited with line ending.
        pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));

        //tell Netty to use string decoder to decode from bytes to string
        pipeline.addLast("decoder", new StringDecoder());

        //tell Netty to use string encoder to encode string to bytes
        pipeline.addLast("encoder", new StringEncoder());

        pipeline.addLast("handler", new ChatServerHandler());
    }
}
