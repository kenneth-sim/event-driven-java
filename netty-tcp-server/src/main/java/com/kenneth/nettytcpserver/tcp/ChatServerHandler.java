package com.kenneth.nettytcpserver.tcp;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.springframework.stereotype.Component;

@Component
public class ChatServerHandler extends SimpleChannelInboundHandler<String> {
    private static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        for(Channel channel : channels){
            channel.writeAndFlush("[SERVER] - " + ctx.channel().remoteAddress() + " has joined!\n");
        }
        channels.add(ctx.channel());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        for(Channel channel : channels){
            channel.writeAndFlush("[SERVER] - " + ctx.channel().remoteAddress() + " has left!\n");
        }
        channels.remove(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String s) throws Exception {
        for(Channel channel : channels){
            channel.writeAndFlush("[" + ctx.channel().remoteAddress() + "]" + s + "\n");
        }
        ctx.writeAndFlush(s);
    }
}
