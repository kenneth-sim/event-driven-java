package com.kenneth.event.webflux.ws;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WSMsg {
    private int heartBeat;
    private String message;
    private String sessionId;
    private String channel;
}

enum HeartBeat{
    CONNECTED,
    DISCONNECTED,
    REQUEST,
    RESPONSE
}

enum ChannelMap{
    MAIN,
    CRYPTO,
    SPORT,
    NEWS;

    public static ChannelMap isValidChannel(String input) {
        try {
            return ChannelMap.valueOf(input.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}


