package com.kenneth.event.webflux.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/testing")
public class TestingController {

    @Autowired
    private TestingWebSocket testingWebSocket;

    @PostMapping("/{channelMap}")
    public void testingSingleClient(@PathVariable ChannelMap channelMap, @RequestParam String message){
        testingWebSocket.broadcast(message, channelMap);
    }
}
