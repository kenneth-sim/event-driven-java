package com.kenneth.event.webflux.api;

import jakarta.annotation.PostConstruct;
import lombok.Builder;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.time.Duration;
import java.util.stream.IntStream;

@RestController
@EnableScheduling
public class TestingApiController {
    private final Sinks.Many<Product> productSink = Sinks.many().replay().all();
    //    private final Sinks.Many<Product> productSink = Sinks.many().multicast().onBackpressureBuffer();
    private final Flux<Product> productFlux = productSink.asFlux();

    public static final Flux<TestingApiController.Product> productFixedFlux = Flux.just(
            TestingApiController.Product.builder().name("product1").quantity(1).build(),
            TestingApiController.Product.builder().name("product2").quantity(1).build(),
            TestingApiController.Product.builder().name("product3").quantity(1).build(),
            TestingApiController.Product.builder().name("product4").quantity(1).build(),
            TestingApiController.Product.builder().name("product5").quantity(1).build(),
            TestingApiController.Product.builder().name("product6").quantity(1).build()
    );

    @PostConstruct
    public void init() throws InterruptedException {
        IntStream.range(1, 100)
                .forEach(i -> {
                    Product product = Product.builder()
                            .name("item" + i)
                            .quantity(100 + i)
                            .build();
                    Sinks.EmitResult result = productSink.tryEmitNext(product);
                    if (result.isFailure()) {
                        System.err.println("Failed to emit product: " + result);
                    }
                });
    }

    @Scheduled(initialDelay = 30000)
    public void postAction(){
        System.out.println("start post action");
        IntStream.range(1, 100)
                .forEach(i -> {
                    Product product = Product.builder()
                            .name("item10" + i)
                            .quantity(200 + i)
                            .build();
                    Sinks.EmitResult result = productSink.tryEmitNext(product);
                    if (result.isFailure()) {
                        System.err.println("Failed to emit product: " + result);
                    }
                });
    }

    @GetMapping(value = "/products", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Product> getProductStreams(){
        return productFlux
//                .onBackpressureBuffer(10, BufferOverflowStrategy.DROP_OLDEST)
                .delayElements(Duration.ofMillis(100))
                .log();
    }

    @GetMapping(value = "/products-sink-no-wait", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Product> getProductStreamsSinkNoWait(){
        return productFixedFlux.log();
    }



    @Builder
    static class Product{
        public String name;
        public int quantity;
    }
}
