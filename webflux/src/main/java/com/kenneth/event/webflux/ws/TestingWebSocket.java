package com.kenneth.event.webflux.ws;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
@Slf4j
public class TestingWebSocket implements WebSocketHandler {
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Map<ChannelMap, Map<String, Sinks.Many<WSMsg>>> channelSinks = new ConcurrentHashMap<>();
    private final Map<ChannelMap, List<String>> channelPermit = new ConcurrentHashMap<>();

    @Bean
    public SimpleUrlHandlerMapping simpleUrlHandlerMapping(){
        Map<String, WebSocketHandler> urlMap = new HashMap<>();
        urlMap.put("/ws", this);

        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setUrlMap(urlMap);
        handlerMapping.setOrder(1);
        return handlerMapping;
    }

    private void initChannelForClient(String sessionId){
        for (ChannelMap channelMap : ChannelMap.values()){
            Map<String, Sinks.Many<WSMsg>> existingInnerMap = channelSinks.getOrDefault(channelMap, new ConcurrentHashMap<>());
            existingInnerMap.put(sessionId, Sinks.many().multicast().onBackpressureBuffer());
            channelSinks.put(channelMap, existingInnerMap);
        }
        List<String> sessionIds = new ArrayList<>(channelPermit.getOrDefault(ChannelMap.MAIN, new ArrayList<>()).stream().toList());
        sessionIds.add(sessionId);
        channelPermit.put(ChannelMap.MAIN, sessionIds);
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        initChannelForClient(session.getId());

        Mono<Void> ping = session.send(Flux.interval(Duration.ofSeconds(5))
                .map(aLong -> session.pingMessage(
                        dataBufferFactory -> session.bufferFactory().allocateBuffer(1024))
                )
        );

        // transforms the Flux into a hot stream (default cold stream)
        Flux<WebSocketMessage> messageFlux = session.receive().share();

        Flux<WebSocketMessage> pong = messageFlux.filter(webSocketMessage -> webSocketMessage.getType() == WebSocketMessage.Type.PONG)
                .doOnNext(webSocketMessage -> {
                    WSMsg wsMsg = WSMsg.builder().heartBeat(HeartBeat.CONNECTED.ordinal()).message(String.valueOf(Instant.now())).sessionId(session.getId()).channel("MAIN").build();
                    channelSinks.get(ChannelMap.MAIN).get(session.getId()).tryEmitNext(wsMsg);
                });

        for (ChannelMap channelMap : ChannelMap.values()){
            session.send(
                    channelSinks.get(channelMap).get(session.getId()).asFlux().map(wsMsg -> {
                        if(channelPermit.get(channelMap).contains(session.getId())){
                            try {
                                String jsonString = objectMapper.writeValueAsString(wsMsg);
                                return session.textMessage(jsonString);
                            } catch (Exception e) {
                                log.error("Error converting WSMsg to JSON", e);
                                return session.textMessage("");
                            }
                        }
                        return session.textMessage("");
                    })
            ).subscribe();
        }

        Mono<Void> handleIncomingMsg = messageFlux.filter(webSocketMessage -> webSocketMessage.getType() == WebSocketMessage.Type.TEXT)
                .map(WebSocketMessage::getPayloadAsText)
                .doOnNext(message -> {
                    handlingIncomingMsg(session.getId(), message);
                })
                .then();



        return Flux.merge(ping, pong, handleIncomingMsg)
                .doFinally(signal -> {
                    for(Map.Entry<ChannelMap, Map<String, Sinks.Many<WSMsg>>> outerEntry : channelSinks.entrySet()){
                        Map<String, Sinks.Many<WSMsg>> innerMap = outerEntry.getValue();
                        innerMap.remove(session.getId());
                    }

                    for(Map.Entry<ChannelMap, List<String>> entry : channelPermit.entrySet()){
                        List<String> sessionIds = entry.getValue();
                        sessionIds.removeIf(sessionId -> sessionId.equals(session.getId()));
                    }
                })
                .then();
    }

    public void broadcast(String message, ChannelMap channel) {
        ChannelMap channelMap = channel != null ? channel : ChannelMap.MAIN;
        channelSinks.get(channelMap).values().forEach(sink -> {
                sink.tryEmitNext(
                        WSMsg.builder()
                                .heartBeat(HeartBeat.CONNECTED.ordinal())
                                .message(message)
                                .sessionId(null)
                                .channel(channelMap.name())
                                .build()
                );
            }
        );
    }

    private void handlingIncomingMsg(String sessionId, String message){
        if (message.startsWith("subscribe:")) {
            String channel = message.split(":")[1];
            subscribeToChannel(sessionId, channel);
        }else if (message.startsWith("unsubscribe:")){
            String channel = message.split(":")[1];
            unSubscribeToChannel(sessionId, channel);
        }
    }

    private void subscribeToChannel(String sessionId, String channel){
        ChannelMap channelMap = ChannelMap.isValidChannel(channel);
        if (channelMap == null) {
            log.warn("Invalid channel: " + channel + ". Subscription skipped.");
            return;
        }

        List<String> sessionIds = new ArrayList<>(channelPermit.getOrDefault(channelMap, new ArrayList<>()).stream().toList());
        sessionIds.add(sessionId);
        channelPermit.put(channelMap, sessionIds);
        log.info("Session " + sessionId + " subscribed to channel " + channel);
    }

    private void unSubscribeToChannel(String sessionId, String channel){
        ChannelMap channelMap = ChannelMap.isValidChannel(channel);
        if (channelMap == null) {
            log.warn("Invalid channel: " + channel + ". Un-subscription skipped.");
            return;
        }


        List<String> sessionIds = new ArrayList<>(channelPermit.get(channelMap).stream().toList());
        sessionIds.removeIf(sId -> sId.equals(sessionId));
        channelPermit.put(channelMap, sessionIds);
        log.info("Session " + sessionId + " unsubscribed to channel " + channel);
    }

}
