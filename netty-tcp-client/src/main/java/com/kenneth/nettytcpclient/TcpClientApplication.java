package com.kenneth.nettytcpclient;

import com.kenneth.nettytcpclient.tcp.ChatClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class TcpClientApplication {
  public static void main(String[] args) {
    SpringApplication.run(TcpClientApplication.class, args);
  }

  @Bean
  CommandLineRunner run(ChatClient chatClient) {
    return args -> chatClient.run();
  }
}
