package com.kenneth.nettytcpclient.tcp;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class ChatClient{

    @Value("${chatClient.netty.host:localhost}")
    private String host;

    @Value("${chatClient.netty.port:8001}")
    private int port;

    EventLoopGroup group;

    public static Channel channel;

    @PostConstruct
    public void run(){
        new Thread(() -> {
            group = new NioEventLoopGroup();

            try{
                //use Bootstrap class to setup a Channel using EventLoopGroup
                Bootstrap bootstrap = new Bootstrap()
                        .group(group)
                        .channel(NioSocketChannel.class) //use this server socket for communication
                        .handler(new ChatClientInitializer());

                //use the boostrap to create the connection to the server with the host and port
                channel = bootstrap.connect(host, port).sync().channel();

//                //use BufferedReader to capture the user input from the console
//                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
//
//                //write the user input over to the server
//                while(true){
//                    channel.write(in.readLine() + "\r\n");
//                }

                Scanner scanner = new Scanner(System.in);
                while (true) {
                    String msg = scanner.nextLine();
                    if ("exit".equals(msg)) {
                        break;
                    }
                    channel.write(msg + "\r\n");
                }

            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }).start();
    }

    @PreDestroy
    public void destroy() throws InterruptedException {
        if(group != null){
            group.shutdownGracefully().sync();
        }
    }
}
