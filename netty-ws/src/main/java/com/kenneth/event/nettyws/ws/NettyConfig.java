package com.kenneth.event.nettyws.ws;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NettyConfig {

    private static volatile ChannelGroup channelGroup = null;
    private static volatile Map<String, Channel> channelMap = null;

    private static final Object lock1 = new Object();
    private static final Object lock2 = new Object();

    public static ChannelGroup getChannelGroup(){
        if(channelGroup == null){
            synchronized (lock1){
                if (channelGroup == null) {
                    channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
                }
            }
        }
        return channelGroup;
    }

    public static Map<String, Channel> getChannelMap(){
        if(channelMap == null){
            synchronized (lock2){
                if(channelMap == null){
                    channelMap = new ConcurrentHashMap<>();
                }
            }
        }
        return channelMap;
    }

    public static Channel getChannel(String userId){
        if (channelMap == null) {
            return getChannelMap().get(userId);
        }
        return channelMap.get(userId);
    }
}
