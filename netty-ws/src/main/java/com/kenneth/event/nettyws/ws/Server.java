package com.kenneth.event.nettyws.ws;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//source: https://jxausea.medium.com/spring-boot-intergratd-netty-implements-websocket-communication-2302e09cf748

@Component
@Slf4j
public class Server {

    EventLoopGroup bossGroup;
    EventLoopGroup workerGroup;

    @Value("${webSocket.netty.port:8889}")
    private int port;

    @Autowired
    private ServerInitializer serverInitializer;

    @PostConstruct
    public void run(){
        new Thread(() -> {
            try{
                bossGroup = new NioEventLoopGroup();
                workerGroup = new NioEventLoopGroup();

                ServerBootstrap b = new ServerBootstrap();
                b.group(bossGroup, workerGroup)
                        .channel(NioServerSocketChannel.class)
                        .handler(new LoggingHandler(LogLevel.INFO))
                        .childHandler(serverInitializer);

                ChannelFuture f = b.bind(port).sync();
                log.info("server started on {}", f.channel().localAddress());
                f.channel().closeFuture().sync();
            }catch(Exception ex){
                log.error(ex.getMessage());
            }
         }).start();
    }

    @PreDestroy
    public void destroy() throws InterruptedException {
        if (bossGroup != null) {
            bossGroup.shutdownGracefully().sync();
        }

        if (workerGroup != null) {
            workerGroup.shutdownGracefully().sync();
        }
    }

}
