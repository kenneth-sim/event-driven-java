package com.kenneth.event.rabbitmq.services;

import com.kenneth.event.rabbitmq.models.Dummy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitMQServices {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMsgToProducer(Dummy dummy){
        rabbitTemplate.convertAndSend("x-testing", "testing", dummy);
    }

    @RabbitListener(queues ="q-testing")
    public void receiveMsg(Message message){
        if (message == null || message.getBody() == null || message.getBody().length < 1) {
            log.error("oh no");
        }

        
    }
}
