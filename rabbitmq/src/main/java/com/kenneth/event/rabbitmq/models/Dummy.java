package com.kenneth.event.rabbitmq.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Dummy implements Serializable {
    @Serial
    private static final long serialVersionUID = -3837842354919578568L;
    public String name;
    public BigDecimal price;
    public Long ts;
}
